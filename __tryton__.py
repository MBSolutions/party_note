#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Party_Note',
    'name_de_DE': 'Parteien Notizen',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides the possibility to store notes for parties
    ''',
    'description_de_DE': '''
    - Ermöglicht das Hinterlegen von Notizen für Parteien
    ''',
    'depends': [
        'party',
    ],
    'xml': [
        'note.xml',
        'party.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
